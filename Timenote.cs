﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPON_LV1
{
    class Timenote: Note
    {
        private DateTime Time;
        public Timenote()
        {
            this.Time = DateTime.Now;
        }

        public DateTime Time
        {
            get { return this.Time; }
            set { this.Time = value; }
        }

        public Timenote(string Text, string Author, DateTime Time, int Priority)
        {
            this.Time = time;

        }

        public override string ToString()
        {
            return base.ToString() + this.Time;
        }
    }
}
