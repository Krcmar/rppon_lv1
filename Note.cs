﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class Note
    {
        private String Text;
        private String Author;
        private int Priority;

        public string Text
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        public string Author
        {
            get { return this.Author; }
            private set { this.Author = value; }
        }

        public int Priority
        {
            get{ return this.Priority; }
            set { this.Priority = value; }
        }

        public String getText()
        {
            return this.Text;
        }

        public String getAuthor()
        {
            return this.Author;
        }

        public int getPriority()
        {
            return this.Priority;
        }

        public void setText(String Text)
        {
            this.Text = Text;
        }

        public void setPriority(int Priority)
        {
            this.Priority = Priority;
        }

        public Note()
        {
            this.Text = "I am Pero Peric.";
            this.Author = "Pero Peric";
            this.Priority = 5;
        }

        public Note(string Text)
        {
            this.Text = Text;
            this.Author = "Pero Peric";
            this.Priority = 5;
        }

        public Note(string Text, int Priority)
        {
            this.Text = Text;
            this.Author = "Pero Peric";
            this.Priority = Priority;
        }

        public override string ToString()
        {
            return this.Text + this.Author + this.Priority;
        }
    }
}
